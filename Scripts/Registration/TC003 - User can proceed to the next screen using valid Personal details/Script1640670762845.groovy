import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Registration/Textfield-firstName'), GlobalVariable.ShortTimeOut)

Mobile.setText(findTestObject('Registration/Textfield-firstName'), 'test123!', GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Registration/Textfield-surname'), GlobalVariable.ShortTimeOut)

Mobile.setText(findTestObject('Registration/Textfield-surname'), 'test123!', GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Dropdown - Countrycode'), GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Btn - Fil CCode'), GlobalVariable.ShortTimeOut)

Mobile.tapAndHold(findTestObject('Registration/Textfield - Mobile No'), 0, GlobalVariable.ShortTimeOut)

Mobile.sendKeys(findTestObject('Registration/Textfield - Mobile No'), '9177848438', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Registration/Textfield - Email'), GlobalVariable.ShortTimeOut)

Mobile.setText(findTestObject('Object Repository/Registration/Textfield - Email'), 'testmail@mail.com', GlobalVariable.ShortTimeOut)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Registration/Dropdown - Birthdate'), GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Btn - Birthdate Year'), GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Btn - Year 2008'), GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Btn - Dec 1'), GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Btn - Birthdate Picker OK'), GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Dropdown - Nationality'), GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Option - American'), GlobalVariable.ShortTimeOut)

Mobile.swipe(0, 900, 0, 100)

Mobile.tap(findTestObject('Registration/Btn - Continue Personal Details'), GlobalVariable.ShortTimeOut)

Mobile.delay(GlobalVariable.ShortTimeOut, FailureHandling.STOP_ON_FAILURE)

