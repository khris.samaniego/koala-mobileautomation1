import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Object Repository/Registration/Textfield - Password'), GlobalVariable.ShortTimeOut)

Mobile.setText(findTestObject('Object Repository/Registration/Textfield - Password'), 'test123!', GlobalVariable.ShortTimeOut)

Mobile.tap(findTestObject('Object Repository/Registration/Textfield - Reenter Password'), GlobalVariable.ShortTimeOut)

Mobile.setText(findTestObject('Object Repository/Registration/Textfield - Reenter Password'), 'test123!', GlobalVariable.ShortTimeOut)

Mobile.hideKeyboard()

Mobile.swipe(0, 900, 0, 100)

Mobile.tapAndHold(findTestObject('Registration/Checkbox - TandC'), GlobalVariable.ShortTimeOut, 0)

Mobile.tap(findTestObject('Object Repository/Registration/Btn - Continue Nominate Password'), GlobalVariable.ShortTimeOut)

Mobile.delay(GlobalVariable.ShortTimeOut, FailureHandling.STOP_ON_FAILURE)