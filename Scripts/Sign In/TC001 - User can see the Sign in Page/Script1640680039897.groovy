import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('/Users/khrissamaniego/git/koala-mobileautomation1/Data Files/Koala QAT_1.0.0 (2).apk', false)

Mobile.verifyElementVisible(findTestObject('Registration/Image-Splash Screen'), GlobalVariable.ShortTimeOut)

Mobile.delay(GlobalVariable.ShortTimeOut, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Registration/Btn-Close Promotion Modal'), 0)

Mobile.delay(GlobalVariable.ShortTimeOut, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Registration/Btn-Sign In'), GlobalVariable.ShortTimeOut)

Mobile.verifyElementVisible(findTestObject('Object Repository/Sign In/Text - Welcome to Robinsons Text - Welcome to RHR'), 
    GlobalVariable.ShortTimeOut)

Mobile.verifyElementVisible(findTestObject('Object Repository/Sign In/Text - Continue with Google'), GlobalVariable.ShortTimeOut)

Mobile.verifyElementVisible(findTestObject('Object Repository/Sign In/Text - Continue with Facebook'), GlobalVariable.ShortTimeOut)

Mobile.verifyElementVisible(findTestObject('Object Repository/Sign In/Text - Continue with Apple'), GlobalVariable.ShortTimeOut)

Mobile.verifyElementVisible(findTestObject('Object Repository/Sign In/Btn - Sign up - Sign in page'), GlobalVariable.ShortTimeOut)

