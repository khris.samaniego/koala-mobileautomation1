<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Testing</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>944721e3-54db-4de0-913d-f179925de0ef</testSuiteGuid>
   <testCaseLink>
      <guid>755137e3-bec3-410e-a634-ff0f37dbea65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC001 - User can see the sign up pop up modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94e7b5d8-aea2-4055-814d-54d35e8a2191</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC002 - User cannot proceed to the next screen if Personal Details textfield are empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a95537a-8957-42af-aa5f-18569356d507</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC003 - User can proceed to the next screen using valid Personal details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9ad0290-3c08-4c02-8044-f12ab1a79014</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC004 - User can skip the Additional Information screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b13f031-aec9-4bbf-bf78-9c4beddcf500</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC005 - User can successfully nominate password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63575923-1252-4df4-98f5-2050199a0c50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC006 - User can verify email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>273f6bf0-7ac5-439c-9580-8d7efaffedd1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC007 - User can navigate back to the landing screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68081478-7918-41fa-a5cb-9a7c8e6efbfa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Sign In/TC001 - User can see the Sign in Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef902547-bf40-4ec3-8782-ae416f276ab9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Sign In/TC002 - User cannot Sign if required fields are empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e5166a1-d0cd-48b6-a75b-06c7e3bedce9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Sign In/TC003 - User can Sign in using valid details</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
